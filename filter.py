from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.svm import SVC
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer, TfidfVectorizer
from utils import *
from estimator_helper import *
from sklearn import linear_model, pipeline
from sklearn.metrics import make_scorer
from sklearn.tree import DecisionTreeClassifier
from sklearn.naive_bayes import MultinomialNB, GaussianNB, BernoulliNB
from sklearn.model_selection import GridSearchCV
import numpy as np
from collections import Counter
import matplotlib.pyplot as plt
from plotting import *

def save_data(filename, lines):
    f = open(filename, "w+")
    for line in lines:
        f.write(str(line) + "\n")
    f.close()


def predict(filter1, X):
    """Produce predictions for X using given filter.
    Please keep the same arguments: X, y (to be able to import this function for evaluation)
    """
    assert len(locals().keys()) == 2
    return filter1.predict(X)


def train_filter(X, y):
    """Return a trained spam filter.
    Please keep the same arguments: X, y (to be able to import this function for evaluation)
    """
    assert 'X' in locals().keys()
    assert 'y' in locals().keys()
    assert len(locals().keys()) == 2

    steps = [('vect', CountVectorizer(preprocessor=preprocessor)),
            ('tfidf', TfidfTransformer()),
            (key, model)]
    pipe = pipeline.Pipeline(steps)
    our_scorer = make_scorer(modified_accuracy, greater_is_better=True)
    parameters = {
        'vect__min_df': (2, 5, 10),
        'vect__max_df': (0.7, 0.8, 0.9),
        'vect__ngram_range': ((1, 1), (1, 2), (1, 4)),
    }

    grid_search = GridSearchCV(pipe, parameters, cv=5, n_jobs=-1, verbose=1, scoring=our_scorer)
    grid_search.fit(X, y)
    return grid_search


def find_best_estimator(X_train, X_test, y_train, y_test):
    models = {
        'abc': AdaBoostClassifier(),
        # 'dtc': DecisionTreeClassifier(),
        # 'mnb': MultinomialNB(),
        'bnb': BernoulliNB(),
        'SVC': SVC()
    }
    params = {
        'abc': {'abc__n_estimators': [150],
                'abc__base_estimator': [DecisionTreeClassifier(max_depth=2),
                                        # DecisionTreeClassifier(max_depth=4),
                                        # DecisionTreeClassifier(max_depth=10), None]},
                                        ]},
        'dtc': {'dtc__max_depth': [None, 5, 10, 20]},
        'mnb': {},
        'bnb': {},
        'SVC': {'SVC__kernel': ['linear'],
                'SVC__C': [10]},
    }
    selector = EstimatorSelectionHelper(models, params)
    selector.fit(X_train, y_train)
    return selector.compare_estimators(X_test, y_test)


if __name__ == '__main__':
    X_train, X_test, y_train, y_test = load_data()
    models = find_best_estimator(X_train, X_test, y_train, y_test)
    for key, model in models.items():
        plt.figure()
        plot_learning_curve(model, key, X_train, y_train)
        plt.show()
        plt.savefig("training_curve" + key + ".eps")
    for key, model in models.items():
        plt.figure()
        plot_learning_curve(model, key, X_test, y_test)
        plt.show()
        plt.savefig("validation_curve" + key + ".eps")

    print("DONE")



