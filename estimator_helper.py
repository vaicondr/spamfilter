from utils import *
from sklearn import linear_model, pipeline, svm
from sklearn.metrics import confusion_matrix, make_scorer
from sklearn.model_selection import GridSearchCV
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from text_sanitizer import *
from sklearn.metrics import roc_curve, auc
from plotting import *

vectorizer_parameters = {
    'vect__min_df': (2, 5, 10),
    'vect__max_df': (0.7, 0.8),
    'vect__ngram_range': ((1, 1), (1, 2), (1, 4)),
}

class EstimatorSelectionHelper:
    def __init__(self, models, params):
        self.grid_searches = {}
        self.parameters = {}
        for key, model in models.items():
            steps = [('vect', CountVectorizer(preprocessor=preprocessor)),
                     ('tfidf', TfidfTransformer()),
                     (key, model)]
            pipe = pipeline.Pipeline(steps)
            our_scorer = make_scorer(modified_accuracy, greater_is_better=True)
            self.parameters[key] = {}
            self.parameters[key].update(vectorizer_parameters)
            if isinstance(params[key], list):
                for param in params[key]:
                    self.parameters[key].update(param)
            else:
                self.parameters[key].update(params[key])
            grid_search = GridSearchCV(pipe, self.parameters[key], cv=5, n_jobs=-1, verbose=1, scoring=our_scorer)
            self.grid_searches[key] = grid_search

    def fit(self, X, y):
        for key, model in self.grid_searches.items():
            print("Running GridSearchCV for %s." % key)
            model.fit(X, y)
            print("Best parameters set:")
            best_parameters = model.best_estimator_.get_params()
            for param_name in sorted(self.parameters[key].keys()):
                print("\t%s: %r" % (param_name, best_parameters[param_name]))

            print('accuracy on training data: ')
            print(model.score(X, y))

    def compare_estimators(self, X_test, y_test):
        for key, model in self.grid_searches.items():
            print("Testing for %s." % key)
            print('accuracy on testing data: ')
            print(model.score(X_test, y_test))
        return self.grid_searches



