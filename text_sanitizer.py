from bs4 import BeautifulSoup
from email import policy
from email.parser import Parser
from html import unescape
import re


def preprocessor(doc):
    # content = parse_content(doc)
    # content = sanitize(content)
    return unescape(doc).lower()


def clean_html(html):
    soup = BeautifulSoup(html, features="html.parser")
    for s in soup(['script', 'style']):
        s.decompose()
    return ' '.join(soup.stripped_strings).lower()


def find_urls(string):
    url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', string)
    return url


def replace_urls(string):
    urls = find_urls(string)
    for url in urls:
        string = string.replace(url, "URL")
    return string


def replace_numbers(string):
    words = string.split(' ')
    for word in words:
        if word.isdigit():
            string = string.replace(word, "NUM")
            continue
    return string


def remove_special_characters_and_spaces(string):
    # remove all single characters
    sanitized = re.sub(r'\s+[a-zA-Z]\s+', ' ', string)
    # Remove single characters from the start
    sanitized = re.sub(r'\^[a-zA-Z]\s+', ' ', sanitized)
    # Substituting multiple spaces with single space
    sanitized = re.sub(r'\s+', ' ', sanitized, flags=re.I)
    return sanitized


def sanitize(string):
    # # Remove all the special characters
    cleaned = clean_html(string)
    sanitized = remove_special_characters_and_spaces(cleaned)
    sanitized = replace_urls(sanitized)
    sanitized = replace_numbers(sanitized)
    return sanitized


# Returns only the text content from email message
def parse_content(message_file):
    msg = Parser(policy=policy.default).parsestr(message_file)
    content = ""
    for key in msg.keys():
        del msg[key]
    if msg.is_multipart():
        for part in msg.walk():
            for key in part.keys():
                del part[key]
            content += convert_email_to_string(part)
    content += convert_email_to_string(msg)
    return content


# Tries to convert email to string, returns "" if email is empty. This can happen for multipart messsages.
def convert_email_to_string(email):
    try:
        return str(email)
    except:
        return ""



