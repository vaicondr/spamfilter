from sklearn.feature_extraction.text import CountVectorizer
from html import unescape
import spacy


class CustomVectorizer(CountVectorizer):
    def build_preprocessor(self):
        def preprocessor(doc):
            return (unescape(doc).lower())
        return preprocessor

    def build_analyzer(self):
        stop_words = self.get_stop_words()
        def analyser(doc):
            spacy.load('en')
            lemmatizer = spacy.lang.en.English()
            doc_clean = unescape(doc).lower()
            tokens = lemmatizer(doc_clean)
            lemmatized_tokens = [token.lemma_ for token in tokens]
            return (self._word_ngrams(lemmatized_tokens, stop_words))

        return analyser
